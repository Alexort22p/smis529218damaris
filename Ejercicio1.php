<?php

class claseCelda
{
    //propiedades de las celdas
    private $colorFondo;
    private $colorLetra;
    private $tipoLetra;
    private $tamanoLetra;


    //metodo  contructor de la clase claseCelda
    public function __construct($bgcolor, $fgcolor, $fontfam, $fontsize)
    {
        $this->colorFondo = $bgcolor;
        $this->colorLetra =$fgcolor;
        $this->tipoLetra =$fontfam;
        $this->tamanoLetra =$fontsize;
    }

    //metodos para configurar la celda

    public function pinta_celda($contenido)
    {
        $celda = "<td style=\"font-family:{$this->tipoLetra};";
        $celda .="font-size:{$this->tamanoLetra};";
        $celda .="padding:12px 16px;";
        $celda .="background:{$this->colorFondo};";
        $celda .="color:{$this->colorLetra};\">\n";
        $celda .= utf8_decode($contenido)."\n";
        $celda .="</td>\n";
        echo $celda;
    }
        
     }

     if (isset($_POST['enviar'])){
        $content = isset($_POST['content']) ? $_POST['content']:"";
        if ($content =="") {
            $msg ="<h3>Debe escribir un nombre </h3>";
            $msg .="<a href=\"Ejercicio1.html\">Regresar al formulario</a>";
            die($msg);
        }

        $bgcolor = isset($_POST['bgcolor']) ? $_POST['bgcolor']:"";

        $fgcolor = isset($_POST['fgcolor']) ? $_POST['fgcolor']:"";

        $fontfam = isset($_POST['font']) ? $_POST['font']:"";


        $fontsize = isset($_POST['size']) ? $_POST['size']:"";

        $nuevacelda = new claseCelda($bgcolor, $fgcolor, $fontfam, $fontsize);
        echo "<table>\n<tr>\n";
        $nuevacelda->pinta_celda($content);
        echo "</tr>\n</table>\n";
    
    }
?>