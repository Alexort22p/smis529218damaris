<?php 

class Empresa{
    private $pago;
    private $renta;
    private $descuento;
    private $horaextra;
    private $totalpago;
    private $seguroS;
    private $afp;
  


    public function getTotalPag():float{
        return $this->totalpago;
    }	

    public function setPagTotal(float $TOTAL){
        $this->totalpago=$TOTAL;
    }
    public function getPago():float{
        return $this->pago;
    }	
    public function set(float $SUELDO){
        $this->pago=$SUELDO;
    }	
    
    public function getSeguroSocial():float{
        return $this->seguroS;
    }	
    public function setSeguro(float $segurosocialISSS){
        $this->seguroS=$segurosocialISSS;
    }	

    public function getRenta():float{
        return $this->renta;
    }	
    public function setRenta(float $RENTA){
        $this->renta=$RENTA;
    }

    public function getHoraExtra():float{
        return $this->horaextra;
    }	

    public function setHoraExtra(float $HORASEX){
        $this->horaextra=$HORASEX;
    }


    public function getDescuentos():float{
        return $this->descuento;
    }	

    public function setlDescuentos(float $descuento){
        $this->descuento=$descuento;
    }
 
 
    public function getdesAfp():float{
        return $this->afp;
    }	
    public function setAfp(float $DESAFP){
        $this->afp=$DESAFP;
    }	

  

     public function Descuentostotal(){
        $totalDes = $this->afp + $this->seguroS + $this->renta;
        $this->setDescuentototal($totalDes);
    }

    public function DescuentoAFP(float $AFP){
        $desAFP = $this->pago * $AFP ;
        $this->setAfp($desAFP);
    }

    public function DescuentoISS(float $ISSS){
        $desISSS = $this->pago * $ISSS ;
        $this->setSeguro($desISSS);
    }


    

    public function CRenta(){
        $renta = 0;
        $coutafija = 0;
        $exceso = 0;

        if($this->pago >=0.01 && $this->pago<=472.00){
            $renta = 0;
            $coutafija = 0;
            $retencion = 0;
        }else if($this->pago >=472.01 && $this->pago <=895.24){
            $coutafija = 17.67;
            $retencion = ($this->pago - 472) * 0.1;
            $renta = $retencion + $coutafija;
        }else if($this->pago>=895.25 && $this->pago <=2038.10){
            $coutafija = 60;
            $retencion = $this->pago - 895.25 * 0.2;
            $renta = $retencion + $coutafija;   
        }else{
            $coutafija = 288.57;
            $retencion = $this->pago- 2038.11 * 0.3;
            $renta = $retencion + $coutafija;   
        }
        $this->setRenta($renta);
    }

    public function TotalPago(){
        $totalsal = $this->pago+ $this->horaextra - $this->descuento;
        $this->setTotalPago($pago);
    }

    public function HorasExtras(float $HORASEXTRAS){
        $abono= 10;
        $total = $HORASEXTRAS * $abono;
        $this->setHorasExtras($total);
    }
  
  
    
}
$boletapago = new Empleado();
$boletapago->setpago(300.00);
$boletapago->DescuentoISS(0.075);
$boletapago->DescuentoAFP(0.035);
$boletapago->HorasExtras(5);
$boletapago->CRenta(); 
$boletapago->TotalDescuentos();
$boletapago->TotalSalario();
echo "El sueldo del empleado es de:" , $boletapago->getPago();
echo "</br> Descuento de ISSS es de: ",$boletapago->getSeguroSocial();
echo "</br> Descuento de AFP es de:" , $boletapago->getdesAfp();
echo "</br> Descuento de renta es de: " , $boletapago->getRenta();
echo "</br> Total de descuentos: " , $boletapago->getDescuentos();
echo "</br> Total de pago por horas extras: " , $boletapago->getHoras();
echo "</br>  Sueldo liquido del empleado es de: " , $boletapago->getTotalSal();
?>